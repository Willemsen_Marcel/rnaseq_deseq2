library(Cairo)
width=1024
height=768

# library("pasilla")
# pasCts <- system.file("extdata",
#                       "pasilla_gene_counts.tsv",
#                       package="pasilla", mustWork=TRUE)
# pasAnno <- system.file("extdata",
#                        "pasilla_sample_annotation.csv",
#                        package="pasilla", mustWork=TRUE)
# 
# cts <- as.matrix(read.csv(pasCts,sep="\t",row.names="gene_id"))
# coldata <- read.csv(pasAnno, row.names=1)
# coldata <- coldata[,c("condition","type")]
# coldata$condition <- factor(coldata$condition)
# coldata$type <- factor(coldata$type)
# 
# rownames(coldata) <- sub("fb", "", rownames(coldata))
# all(rownames(coldata) %in% colnames(cts))
# 
# cts <- cts[, rownames(coldata)]
# all(rownames(coldata) == colnames(cts))


# O2358T2 02-001	02-003 01-128	01-180
# counts = read.delim(file = "/Users/marcelwillemsen/Documents/workspace_Molenaar/RNAseq_DESeq2/DATA/MRT_counts.txt", sep = "\t", col.names = T, row.names = F)
counts = read.delim(file = "/Users/marcelwillemsen/Documents/workspace_Molenaar/RNAseq_DESeq2/DATA/MRT_counts.txt", sep = "\t")

# head(paste(counts$EnsemblId, counts$Gene, sep = "_"))
rownames(counts) = paste(counts$EnsemblId, counts$Gene, sep = "_")
counts = counts[,-which(colnames(counts) %in% c("EnsemblId","Gene","O2358T2","X02.001","X02.003","X01.128","X01.180"))]
colnames(counts)[51:52] = c("01-105","Organoid_01-105")

correct = read.delim(file = "/Users/marcelwillemsen/Documents/workspace_Molenaar/RNAseq_DESeq2/DATA/Naming_samples_correct.txt", sep = "\t")
correct = t(correct)
colnames(correct) = correct[1,1]
correct = correct[-1,,drop=F]
dim(correct)
rownames(correct)[51:52] = c("01-105","Organoid_01-105")
cbind(rownames(correct), colnames(counts))
old = colnames(counts)
barplot(which(colnames(counts) %in% rownames(correct)))
barplot(which(rownames(correct) %in% colnames(counts)))
colnames(counts) = as.vector(correct[,1])

meta = read.csv(file = "/Users/marcelwillemsen/Documents/workspace_Molenaar/RNAseq_DESeq2/DATA/coldata.txt", sep = "\t", quote = "")
meta = as.data.frame(t(meta))
colnames(meta) = c("type","tumor_type")
meta$type = factor(meta$type)
meta$tumor_type = factor(meta$tumor_type)
rownames(meta)[c(51,52)] = c("01-105","Organoid_01-105")
cbind(rownames(meta), rownames(correct))
meta = meta[rownames(correct),]
rownames(meta) = as.vector(correct[,1])
meta = meta[colnames(counts),]

dim(meta)
dim(counts)
all(rownames(meta) %in% colnames(counts))
cbind(rownames(meta),colnames(counts))

####################
index = which(rownames(counts) %in% topDEgenes)
counts = counts[-index,]
####################

# c("EnsemblId","Gene","O2358T2","02-001","02-003","01-128","01-180")
library("DESeq2")
dds <- DESeqDataSetFromMatrix(countData = counts,
                              colData = meta,
                              design = ~ type)
dds
saveRDS(dds, file = "./DATA/counts.rds")
# dds = readRDS("./DATA/counts.rds")

# filtering
keep <- rowSums(counts(dds)) >= 10
dds <- dds[keep,]

# setting contrast
# res <- results(dds, contrast=c("condition","treated","untreated"))
# res <- results(dds, contrast=c("type","Tissue","Organoids))

# DE
dds <- DESeq(dds)
res <- results(dds)
res

# Shrinkage of effect size (LFC estimates) is useful for visualization and ranking of genes
library("apeglm")
resultsNames(dds)
resLFC <- lfcShrink(dds, coef="type_Tissue_vs_Organoids", type="apeglm")
resLFC

resOrdered <- resLFC[order(resLFC$pvalue),]

summary(resOrdered)
sum(resOrdered$padj < 0.1, na.rm=TRUE)

# # # Default alpha is set to 0.1
# res05 <- results(dds, alpha=0.05)
# summary(res05)

topDEgenes = rownames(resOrdered)[1:5000] 

dds = readRDS("./DATA/counts.rds")


###############################################################

# # this gives log2(n + 1)
# ntd <- normTransform(dds)
library("vsn")
# meanSdPlot(assay(ntd))

vsd <- vst(dds, blind=FALSE)
meanSdPlot(assay(vsd))

# # too many samples for this
# rld <- rlog(dds, blind=FALSE)
# meanSdPlot(assay(rld))


# error in evaluating the argument 'x' in selecting a method for function 'rowMeans':
#   first calculate size factors, add normalizationFactors,
# or set normalized=FALSE

# TODO normalization
# estimateSizeFactors


library("pheatmap")
# select <- order(rowMeans(counts(dds,normalized=FALSE)), decreasing=TRUE)[1:50]
select <- order(rowVars(counts(dds,normalized=FALSE)), decreasing = T)[1:100]
df <- as.data.frame(colData(dds)[,c("type","tumor_type")])

CairoPNG(filename="./RESULTS/heatmap_top100_v3.png", width, height)
pheatmap(assay(vsd)[select,], cluster_rows=TRUE, show_rownames=FALSE,
         cluster_cols=TRUE, annotation_col=df)
dev.off()



sampleDists <- dist(t(assay(vsd)))
library("RColorBrewer")
sampleDistMatrix <- as.matrix(sampleDists)
rownames(sampleDistMatrix) <- paste(vsd$type, vsd$tumor_type, sep="-")
colnames(sampleDistMatrix) <- NULL
colors <- colorRampPalette( rev(brewer.pal(9, "Blues")) )(255)
pheatmap(sampleDistMatrix,
         clustering_distance_rows=sampleDists,
         clustering_distance_cols=sampleDists,
         col=colors)

library(ggplot2)
library(ggrepel)

# plotPCA(vsd, intgroup=c("type", "tumor_type"))
# vsd$label = c(rep(FALSE,50), rep(TRUE,2))
CairoPNG(filename="./RESULTS/pca-top5000DE_new_label.png", width, height)
z <- plotPCA(vsd, intgroup=c("type", "tumor_type"))
## replot, obscuring points with the sample name
# z + geom_label(aes(label = name)) +
z +  geom_text_repel(aes(label = name)) +
# geom_point() + 
theme_minimal()
dev.off()

